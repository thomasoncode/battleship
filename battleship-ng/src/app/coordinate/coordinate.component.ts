import { Component, OnInit, Input, OnDestroy } from "@angular/core";
import { GridService, GridCoordinateValue, Ship } from "../grid.service";
import { pluck } from "rxjs/operators";
import { Coordinate } from "../coordinate.model";
import { Subscription } from "rxjs";

@Component({
  selector: "app-coordinate",
  templateUrl: "./coordinate.component.html",
  styleUrls: ["./coordinate.component.scss"]
})
export class CoordinateComponent implements OnInit, OnDestroy {
  @Input() rowIndex: number;
  @Input() columnIndex: number;

  private coordinate: Coordinate;
  private value: GridCoordinateValue = null;
  private gridSubscription: Subscription;

  constructor(private gridService: GridService) {}

  ngOnInit(): void {
    this.coordinate = Coordinate.fromIndices({
      rowIndex: this.rowIndex,
      columnIndex: this.columnIndex
    });

    this.gridSubscription = this.gridService.grid$
      .pipe(pluck(this.coordinate.toString()))
      .subscribe(value => (this.value = value));
  }

  get classes() {
    if (this.value) {
      const { ship } = this.value;

      return {
        "aircraft-carrier": ship == Ship.AircraftCarrier,
        battleship: ship == Ship.Battleship
      };
    }

    return "";
  }

  get text() {
    if (this.value) {
      const { ship } = this.value;

      return ship[0];
    }

    return "";
  }

  ngOnDestroy() {
    this.gridSubscription.unsubscribe();
  }
}
