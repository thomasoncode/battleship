import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { Coordinate } from "../coordinate.model";

@Component({
  selector: "app-end-coordinate-select",
  templateUrl: "./end-coordinate-select.component.html",
  styleUrls: ["./end-coordinate-select.component.scss"]
})
export class EndCoordinateSelectComponent implements OnInit {
  @Input() startCoordinate: string = null;
  @Output() coordinateSelected = new EventEmitter<string>();

  coordinate: string;

  constructor() {}

  get coordinates() {
    const coordinate = Coordinate.fromString(this.startCoordinate);
    if (coordinate) {
      const coordinates = coordinate.getCoordinatesNumberOfSpacesAway(5);
      return coordinates;
    }

    return [];
  }

  ngOnInit(): void {}

  onChange(coordinate: string) {
    this.coordinateSelected.emit(coordinate);
  }
}
