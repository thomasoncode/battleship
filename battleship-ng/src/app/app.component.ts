import { Component } from "@angular/core";
import { FormGroup, FormControl } from "@angular/forms";
import { Coordinate } from "./coordinate.model";
import { GridService, Ship } from "./grid.service";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"]
})
export class AppComponent {
  title = "battleship-ng";

  ac: {
    startCoordinate: string;
    endCoordinate: string;
  } = {
    startCoordinate: "",
    endCoordinate: ""
  };

  get indices() {
    return [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];
  }

  constructor(private gridService: GridService) {}

  getColumnHeaderStyle(columnIndex: number) {
    // add two to start after the row header
    const gridColumnStart = columnIndex + 2;

    return {
      "grid-column": `${gridColumnStart} / ${gridColumnStart + 1}`,
      "grid-row": `1 / 1`
    };
  }

  getRowHeaderStyle(rowIndex: number) {
    // add two to start after the column header
    const gridRowStart = rowIndex + 2;

    return {
      "grid-column": `1 / 1`,
      "grid-row": `${gridRowStart} / ${gridRowStart + 1}`
    };
  }

  getCoordinateStyle(rowIndex, columnIndex) {
    // add two to start after the column and row headers
    const gridColumnStart = columnIndex + 2;
    const gridRowStart = rowIndex + 2;

    return {
      "grid-column": `${gridColumnStart} / ${gridColumnStart + 1}`,
      "grid-row": `${gridRowStart} / ${gridRowStart + 1}`
    };
  }

  onEndCoordinateSelected(coordinate: string) {
    this.ac.endCoordinate = coordinate;
  }

  onAddAircraftCarrier() {
    this.gridService.addShip(this.ac.startCoordinate, this.ac.endCoordinate);
  }

  onRemoveAircraftCarrier() {
    this.gridService.removeShip(Ship.AircraftCarrier);
  }
}
