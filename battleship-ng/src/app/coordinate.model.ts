const mapRowLetterToRowIndex = {
  A: 0,
  B: 1,
  C: 2,
  D: 3,
  E: 4,
  F: 5,
  G: 6,
  H: 7,
  I: 8,
  J: 9
};

const mapRowIndexToRowLetter = {
  0: "A",
  1: "B",
  2: "C",
  3: "D",
  4: "E",
  5: "F",
  6: "G",
  7: "H",
  8: "I",
  9: "J"
};

const mapColumnNumberToColumnIndex = {
  "1": 0,
  "2": 1,
  "3": 2,
  "4": 3,
  "5": 4,
  "6": 5,
  "7": 6,
  "8": 7,
  "9": 8,
  "10": 9
};

const mapColumnIndexToColumnNumber = {
  0: "1",
  1: "2",
  2: "3",
  3: "4",
  4: "5",
  5: "6",
  6: "7",
  7: "8",
  8: "9",
  9: "10"
};

function stringIsNotEmpty(value: string) {
  return value.length > 0;
}

class Indices {
  constructor(readonly rowIndex: number, readonly columnIndex: number) {}

  static fromColumnIndex(columnIndex: number) {
    return function fromRowIndex(rowIndex: number) {
      return new Indices(rowIndex, columnIndex);
    };
  }

  static fromRowIndex(rowIndex: number) {
    return function fromColumnIndex(columnIndex: number) {
      return new Indices(rowIndex, columnIndex);
    };
  }
}

function createCoordinateString({ columnIndex, rowIndex }: Indices): string {
  if (
    columnIndex in mapColumnIndexToColumnNumber &&
    rowIndex in mapRowIndexToRowLetter
  ) {
    let coordinate = "";
    coordinate += mapRowIndexToRowLetter[rowIndex];
    coordinate += mapColumnIndexToColumnNumber[columnIndex];

    return coordinate;
  }

  return "";
}

export class Coordinate {
  private constructor(
    readonly rowIndex: number,
    readonly columnIndex: number
  ) {}

  static fromIndices({ rowIndex, columnIndex }: Indices): Coordinate {
    return new Coordinate(rowIndex, columnIndex);
  }

  static fromString(coordinate: string): Coordinate {
    if (coordinate == null || coordinate.length < 2) {
      return null;
    }

    const rowLetter = coordinate.slice(0, 1).toUpperCase();
    const columnNumber = coordinate.slice(1);

    if (
      rowLetter in mapRowLetterToRowIndex &&
      columnNumber in mapColumnNumberToColumnIndex
    ) {
      const rowIndex = mapRowLetterToRowIndex[rowLetter];
      const columnIndex = mapColumnNumberToColumnIndex[columnNumber];

      return new Coordinate(rowIndex, columnIndex);
    }

    return null;
  }

  getCoordinatesNumberOfSpacesAway(numberOfSpaces: number): string[] {
    // subtract one to include the current coordinate
    numberOfSpaces -= 1;

    const coordinates = [
      new Indices(this.rowIndex - numberOfSpaces, this.columnIndex), // top
      new Indices(this.rowIndex, this.columnIndex + numberOfSpaces), // right
      new Indices(this.rowIndex + numberOfSpaces, this.columnIndex), // bottom
      new Indices(this.rowIndex, this.columnIndex - numberOfSpaces) // left
    ];

    return coordinates.map(createCoordinateString).filter(stringIsNotEmpty);
  }

  getCoordinatesBetweenThisAndEndCoordinate(endCoordinate: string): string[] {
    const coordinate = Coordinate.fromString(endCoordinate);
    let range = {
      start: 0,
      end: 0
    };
    let fromIndex: (index: number) => Indices;

    if (coordinate.rowIndex == this.rowIndex) {
      fromIndex = Indices.fromRowIndex(this.rowIndex);
      range =
        this.columnIndex < coordinate.columnIndex
          ? { start: this.columnIndex, end: coordinate.columnIndex }
          : { start: coordinate.columnIndex, end: this.columnIndex };
    } else if (coordinate.columnIndex == this.columnIndex) {
      fromIndex = Indices.fromColumnIndex(this.columnIndex);
      range =
        this.rowIndex < coordinate.rowIndex
          ? { start: this.rowIndex, end: coordinate.rowIndex }
          : { start: coordinate.rowIndex, end: this.rowIndex };
    } else {
      throw new Error();
    }

    return this.range(range.start, range.end)
      .map(fromIndex)
      .map(createCoordinateString);
  }

  private range(start: number, inclusiveEnd: number): number[] {
    const length = inclusiveEnd - start + 1;
    return Array.from<number>({ length }).map((_, index) => index + start);
  }

  toString(): string {
    return createCoordinateString(new Indices(this.rowIndex, this.columnIndex));
  }
}
