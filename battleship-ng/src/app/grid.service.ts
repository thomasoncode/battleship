import { Injectable } from "@angular/core";
import { BehaviorSubject } from "rxjs";
import { Coordinate } from "./coordinate.model";

export enum Ship {
  AircraftCarrier = "Aircraft Carrier",
  Battleship = "Battleship"
}

export interface GridCoordinateValue {
  ship: Ship;
}

interface GridCoordinate {
  [key: string]: GridCoordinateValue;
}

@Injectable({ providedIn: "root" })
export class GridService {
  private grid: GridCoordinate = {};
  private ships: { [key: string]: string[] } = {};
  private gridSubject = new BehaviorSubject<GridCoordinate>(this.grid);
  readonly grid$ = this.gridSubject.asObservable();

  addShip(startCoordinate: string, endCoordinate: string) {
    const coordinates = Coordinate.fromString(
      startCoordinate
    ).getCoordinatesBetweenThisAndEndCoordinate(endCoordinate);
    this.grid = coordinates.reduce(function updateGrid(grid, coordinate) {
      grid[coordinate] = {
        ship: Ship.AircraftCarrier
      };

      return grid;
    }, this.grid);

    this.ships = {
      ...this.ships,
      [Ship.AircraftCarrier]: coordinates
    };

    this.gridSubject.next(this.grid);
  }

  removeShip(ship: Ship) {
    const coordinates = this.ships[ship];

    this.grid = Object.entries(this.grid).reduce((grid, [key, value]) => {
      if (!coordinates.includes(key)) {
        grid[key] = value;
      }

      return grid;
    }, {});

    this.gridSubject.next(this.grid);
  }
}
