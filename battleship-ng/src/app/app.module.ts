import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";

import { AppComponent } from "./app.component";
import { CoordinateComponent } from "./coordinate/coordinate.component";
import { EndCoordinateSelectComponent } from "./end-coordinate-select/end-coordinate-select.component";

@NgModule({
  declarations: [
    AppComponent,
    CoordinateComponent,
    EndCoordinateSelectComponent
  ],
  imports: [BrowserModule, FormsModule],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
