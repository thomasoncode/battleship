import { Coordinate } from "./coordinate.model";

describe("Coordinate", () => {
  describe("constructor", () => {
    it("Given the top left coordinate then create the model", () => {
      // arrange
      // act
      const actualResult = Coordinate.fromString("A1");

      // assert
      expect(actualResult.rowIndex).toEqual(0);
      expect(actualResult.columnIndex).toEqual(0);
    });

    it("Given the top right coordinate then create the model", () => {
      // arrange
      // act
      const actualResult = Coordinate.fromString("A10");

      // assert
      expect(actualResult.rowIndex).toEqual(0);
      expect(actualResult.columnIndex).toEqual(9);
    });

    it("Given the bottom right coordinate then create the model", () => {
      // arrange
      // act
      const actualResult = Coordinate.fromString("J10");

      // assert
      expect(actualResult.rowIndex).toEqual(9);
      expect(actualResult.columnIndex).toEqual(9);
    });

    it("Given the bottom left coordinate then create the model", () => {
      // arrange
      // act
      const actualResult = Coordinate.fromString("J1");

      // assert
      expect(actualResult.rowIndex).toEqual(9);
      expect(actualResult.columnIndex).toEqual(0);
    });

    it("Given the bottom left coordinate then create the model", () => {
      // arrange
      // act
      const actualResult = Coordinate.fromString("J1");

      // assert
      expect(actualResult.rowIndex).toEqual(9);
      expect(actualResult.columnIndex).toEqual(0);
    });
  });

  describe("getCoordinatesNumberOfSpacesAway", () => {
    it("Given the top left coordinate then return valid coordinates", () => {
      // arrange
      const expectedResult = jasmine.arrayWithExactContents(["E1", "A5"]);
      const coordinate = Coordinate.fromString("A1");

      // act
      const actualResult = coordinate.getCoordinatesNumberOfSpacesAway(5);

      // assert
      expect(actualResult).toEqual(expectedResult);
    });

    it("Given the top right coordinate then return valid coordinates", () => {
      // arrange
      const expectedResult = jasmine.arrayWithExactContents(["A6", "E10"]);
      const coordinate = Coordinate.fromString("A10");

      // act
      const actualResult = coordinate.getCoordinatesNumberOfSpacesAway(5);

      // assert
      expect(actualResult).toEqual(expectedResult);
    });

    it("Given the bottom right coordinate then return valid coordinates", () => {
      // arrange
      const expectedResult = jasmine.arrayWithExactContents(["J5", "F1"]);
      const coordinate = Coordinate.fromString("J1");

      // act
      const actualResult = coordinate.getCoordinatesNumberOfSpacesAway(5);

      // assert
      expect(actualResult).toEqual(expectedResult);
    });

    it("Given the bottom left coordinate then return valid coordinates", () => {
      // arrange
      const expectedResult = jasmine.arrayWithExactContents(["J6", "F10"]);
      const coordinate = Coordinate.fromString("J10");

      // act
      const actualResult = coordinate.getCoordinatesNumberOfSpacesAway(5);

      // assert
      expect(actualResult).toEqual(expectedResult);
    });
  });

  describe("getCoordinatesBetweenThisAndEndCoordinate", () => {
    it("Given a left to right direction then return the coordinates", () => {
      // arrange
      const coordinate = Coordinate.fromString("A1");
      const expectedResult = jasmine.arrayWithExactContents([
        "A1",
        "A2",
        "A3",
        "A4",
        "A5"
      ]);

      // act
      const actualResult = coordinate.getCoordinatesBetweenThisAndEndCoordinate(
        "A5"
      );

      // assert
      expect(actualResult).toEqual(expectedResult);
    });

    it("Given a right to left direction then return the coordinates", () => {
      // arrange
      const coordinate = Coordinate.fromString("A5");
      const expectedResult = jasmine.arrayWithExactContents([
        "A1",
        "A2",
        "A3",
        "A4",
        "A5"
      ]);

      // act
      const actualResult = coordinate.getCoordinatesBetweenThisAndEndCoordinate(
        "A1"
      );

      // assert
      expect(actualResult).toEqual(expectedResult);
    });

    it("Given a top to bottom direction then return the coordinates", () => {
      // arrange
      const coordinate = Coordinate.fromString("A1");
      const expectedResult = jasmine.arrayWithExactContents([
        "A1",
        "B1",
        "C1",
        "D1",
        "E1"
      ]);

      // act
      const actualResult = coordinate.getCoordinatesBetweenThisAndEndCoordinate(
        "E1"
      );

      // assert
      expect(actualResult).toEqual(expectedResult);
    });

    it("Given a bottom to top direction then return the coordinates", () => {
      // arrange
      const coordinate = Coordinate.fromString("E1");
      const expectedResult = jasmine.arrayWithExactContents([
        "A1",
        "B1",
        "C1",
        "D1",
        "E1"
      ]);

      // act
      const actualResult = coordinate.getCoordinatesBetweenThisAndEndCoordinate(
        "A1"
      );

      // assert
      expect(actualResult).toEqual(expectedResult);
    });
  });
});
