function endBattle() {
  const errorMessage = "Player's fleet is sunk";

  return {
    addShip() {
      throw Error(errorMessage);
    },
    removeShip() {
      throw Error(errorMessage);
    },
    beginBattle() {
      throw Error(errorMessage);
    },
    // battle
    callOutCoordinate() {
      throw Error(errorMessage);
    },
    // confirm state
    canBeginBattle() {
      return false;
    },
    canCallOutCoordinate() {
      return false;
    },
    isFleetSunk() {
      return true;
    }
  };
}

module.exports = endBattle;
