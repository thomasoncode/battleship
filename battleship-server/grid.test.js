const { makeGrid } = require("./grid");

jest.mock("./coordinate.js");
/** @type {jest.Mock} */
// @ts-ignore
const { validateCoordinate } = require("./coordinate");

describe("makeGrid", () => {
  beforeEach(validateCoordinate.mockReset);

  describe("deleteValueAtCoordinate", () => {
    it("given a deleted coordinate then return null", () => {
      // arrange
      const grid = makeGrid();
      validateCoordinate.mockReturnValue(true);
      grid.setValueAtCoordinate("", "A1");
      grid.deleteValueAtCoordinate("A1");

      const expectedResult = null;

      // act
      const actualResult = grid.getValueAtCoordinate("A1");

      // assert
      expect(actualResult).toEqual(expectedResult);
    });

    it("given an invalid coordinate then do not throw an error", () => {
      // arrange
      const grid = makeGrid();
      validateCoordinate.mockReturnValue(false);

      // act
      // assert
      expect(() => grid.deleteValueAtCoordinate("A1")).not.toThrowError();
    });
  });

  describe("getValueAtCoordinate", () => {
    it("given a valid coordinate then return the value", () => {
      // arrange
      validateCoordinate.mockReturnValue(true);

      // act
      const actualResult = makeGrid().getValueAtCoordinate("A1");

      // assert
      expect(actualResult).toBeNull();
    });

    it("given an invalid coordinate then throw an error", () => {
      // arrange
      validateCoordinate.mockReturnValue(false);

      // act, assert
      expect(() =>
        makeGrid().getValueAtCoordinate("K10")
      ).toThrowErrorMatchingSnapshot();
    });
  });

  describe("setValueAtCoordinate", () => {
    it("given a valid coordinate then set the value", () => {
      // arrange
      validateCoordinate.mockReturnValue(true);
      const coordinate = "A1";
      const expectedResult = "value";

      // act
      const grid = makeGrid();
      grid.setValueAtCoordinate(expectedResult, coordinate);
      const actualResult = grid.getValueAtCoordinate(coordinate);

      // assert
      expect(actualResult).toEqual(expectedResult);
    });

    it("given an invalid coordinate then throw an error", () => {
      // arrange
      validateCoordinate.mockReturnValue(false);

      // act, assert
      expect(() =>
        makeGrid().setValueAtCoordinate("K10", {})
      ).toThrowErrorMatchingSnapshot();
    });
  });
});
