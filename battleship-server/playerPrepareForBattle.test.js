const playerPrepareForBattle = require("./playerPrepareForBattle");
const coordinateRange = require("./coordinateRange");
const playerBattle = require("./playerBattle");

jest.mock("./playerBattle");
jest.mock("./coordinateRange");

describe("playerPrepareForBattle", () => {
  beforeEach(jest.resetAllMocks);

  describe("addShip", () => {
    it("Given a ship with current coordinates then throw an error", () => {
      // arrange
      const state = playerPrepareForBattle({
        grid: mockGrid(),
        fleet: mockFleet()
      });

      // act
      // assert
      expect(() => state.addShip(mockShip("shipWithCoordinates"))).toThrowError(
        /previously added/
      );
    });

    it("Given a coordinate occupied by another ship then throw an error", () => {
      // arrange
      const grid = mockGrid();
      grid.getValueAtCoordinate.mockReturnValue({});
      coordinateRange.mockReturnValue(["coordinate"]);

      const state = playerPrepareForBattle({
        grid,
        fleet: mockFleet()
      });

      // act
      // arrange
      expect(() => state.addShip(mockShip())).toThrowError(
        /outside of the grid or colliding/
      );
    });

    it("Given a ship that can be added then add the ship", () => {
      // arrange
      const grid = mockGrid();
      grid.getValueAtCoordinate.mockReturnValue(null);

      const fleet = mockFleet();
      const state = playerPrepareForBattle({
        grid,
        fleet
      });

      coordinateRange.mockReturnValue(["A1"]);

      // act
      state.addShip(mockShip());

      // assert
      expect(grid.setValueAtCoordinate).toBeCalledWith(
        {
          isHit: false,
          shipId: "shipWithoutCoordinates"
        },
        "A1"
      );
      expect(fleet.shipWithoutCoordinates.coordinates).toEqual(["A1"]);
    });
  });

  describe("removeShip", () => {
    it("Given a ship that was not added then do not throw an error", () => {
      // arrange
      const state = playerPrepareForBattle({
        grid: mockGrid(),
        fleet: mockFleet()
      });

      // act
      // assert
      expect(() =>
        state.removeShip("shipWithoutCoordinates")
      ).not.toThrowError();
    });

    it("Given an added ship then remove the ship", () => {
      // arrange
      const grid = mockGrid();
      const fleet = mockFleet();

      const state = playerPrepareForBattle({
        grid,
        fleet
      });

      // act
      state.removeShip("shipWithCoordinates");

      // assert
      expect(grid.deleteValueAtCoordinate.mock.calls[0][0]).toEqual(
        "coordinate"
      );

      expect(fleet.shipWithCoordinates.coordinates).toEqual([]);
    });
  });

  describe("canBeginBattle", () => {
    it("Given a ship without coordinates then return false", () => {
      // arrange
      const state = playerPrepareForBattle({
        fleet: mockFleet(),
        grid: mockGrid()
      });

      // act
      const actualResult = state.canBeginBattle();

      // assert
      expect(actualResult).toBeFalsy();
    });

    it("Given a ship with coordinates then return true", () => {
      // arrange
      const fleet = mockFleet();

      const state = playerPrepareForBattle({
        fleet,
        grid: mockGrid()
      });

      delete fleet.shipWithoutCoordinates;

      // act
      const actualResult = state.canBeginBattle();

      // assert
      expect(actualResult).toBeTruthy();
    });
  });

  describe("beginBattle", () => {
    it("Given a ship without coordinates then throw an error", () => {
      // arrange
      const state = playerPrepareForBattle({
        fleet: mockFleet(),
        grid: mockGrid()
      });

      // act
      // assert
      expect(state.beginBattle).toThrowError(/Player is not ready/);
    });

    it("Given a ship with coordinates then change state", () => {
      // arrange
      const fleet = mockFleet();
      const grid = mockGrid();
      const changeState = jest.fn();

      const state = playerPrepareForBattle({
        fleet,
        grid,
        changeState
      });

      delete fleet.shipWithoutCoordinates;

      // act
      state.beginBattle();

      // assert
      expect(playerBattle).toBeCalledWith({ fleet, grid, changeState });
      expect(changeState).toBeCalled();
    });
  });

  // the following methods are not allowed in this state
  it("canCallOutCoordinate returns false", () => {
    // arrange
    const state = playerPrepareForBattle({
      grid: mockGrid(),
      fleet: mockFleet()
    });

    // act
    const actualResult = state.canCallOutCoordinate();

    // assert
    expect(actualResult).toBeFalsy();
  });

  it("callOutCoordinate throws an error", () => {
    // arrange
    const state = playerPrepareForBattle({
      grid: mockGrid(),
      fleet: mockFleet()
    });

    // act
    expect(state.callOutCoordinate).toThrowError(/Player is not ready/);
  });

  it("isFleetSunk returns false", () => {
    // arrange
    const state = playerPrepareForBattle({
      grid: mockGrid(),
      fleet: mockFleet()
    });

    // act
    expect(state.isFleetSunk()).toBeFalsy();
  });

  function mockGrid() {
    return {
      getValueAtCoordinate: jest.fn(),
      setValueAtCoordinate: jest.fn(),
      deleteValueAtCoordinate: jest.fn()
    };
  }

  function mockFleet() {
    return {
      shipWithoutCoordinates: {
        id: "shipWithoutCoordinates",
        coordinates: [],
        description: "description"
      },
      shipWithCoordinates: {
        id: "shipWithCoordinates",
        coordinates: ["coordinate"],
        description: "description"
      }
    };
  }

  function mockShip(shipId = "shipWithoutCoordinates") {
    return {
      shipId,
      direction: "direction",
      coordinate: "coordinate"
    };
  }
});
