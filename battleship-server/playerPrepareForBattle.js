const { HORIZONTALLY, VERTICALLY } = require("./directions");
const coordinateRange = require("./coordinateRange");
const isObject = require("lodash/isObject");
const isEmpty = require("lodash/isEmpty");
const playerBattle = require("./playerBattle");

function playerPrepareForBattle({ grid, fleet, changeState }) {
  function addShip({ shipId, direction, coordinate }) {
    const ship = fleet[shipId];
    if (isEmpty(ship.coordinates) == false) {
      throw new Error(
        `The ${ship.description} was previously added to the game ` +
          "Remove it from the game and try again"
      );
    }

    const coordinates = coordinateRange({
      direction,
      coordinate,
      numberOfSquares: ship.numberOfSquares
    });

    if (coordinates.map(grid.getValueAtCoordinate).some(isObject)) {
      throw new Error(
        `Placing a(n) ${ship.description} ${direction} ` +
          `at ${coordinate} will result in the ship being addd ` +
          "outside of the grid or colliding with another ship"
      );
    }

    coordinates.forEach(function setValueAtCoordinate(coordinate) {
      grid.setValueAtCoordinate(
        {
          isHit: false,
          shipId: ship.id
        },
        coordinate
      );
    });
    ship.coordinates = coordinates;
  }

  function removeShip(shipId) {
    const ship = fleet[shipId];

    if (isEmpty(ship.coordinates)) {
      return;
    }

    ship.coordinates.forEach(grid.deleteValueAtCoordinate);
    ship.coordinates = [];
  }

  function getShipsWithoutCoordinates() {
    return Object.values(fleet)
      .filter(function emptyCoordinates({ coordinates }) {
        return isEmpty(coordinates);
      })
      .map(function getDescription({ id }) {
        return id;
      });
  }

  function canBeginBattle() {
    return isEmpty(getShipsWithoutCoordinates());
  }

  function beginBattle() {
    if (canBeginBattle() == false) {
      throw new Error(getPlayerNotReadyMessage());
    }

    changeState(playerBattle({ grid, fleet, changeState }));
  }

  function canCallOutCoordinate() {
    return false;
  }

  function callOutCoordinate() {
    throw new Error(getPlayerNotReadyMessage());
  }

  function isFleetSunk() {
    return false;
  }

  function getPlayerNotReadyMessage() {
    return (
      "Player is not ready for battle. " +
      `Add the following ships ${getShipsWithoutCoordinates().join(", ")}`
    );
  }

  return {
    addShip,
    removeShip,
    beginBattle,
    canBeginBattle,
    canCallOutCoordinate,
    callOutCoordinate,
    isFleetSunk
  };
}

module.exports = playerPrepareForBattle;
