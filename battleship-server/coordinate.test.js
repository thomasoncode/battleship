const {
  validateCoordinate,
  convertCoordinateToIndices,
  convertIndicesToCoordinate
} = require("./coordinate");

describe("coordinate", () => {
  describe("getCoordinateFromIndices", () => {
    it("given valid indices then return a coordinate", () => {
      // arrange
      const expectedResult = "E6";

      // act
      const actualResult = convertIndicesToCoordinate({
        rowIndex: 4,
        columnIndex: 5
      });

      // assert
      expect(actualResult).toEqual(expectedResult);
    });

    it("given invalid row index then throw an error", () => {
      // arrange, act, assert
      expect(() =>
        convertIndicesToCoordinate({
          rowIndex: 10,
          columnIndex: 0
        })
      ).toThrowErrorMatchingSnapshot();
    });

    it("given invalid column index then throw an error", () => {
      // arrange, act, assert
      expect(() =>
        convertIndicesToCoordinate({
          rowIndex: 0,
          columnIndex: 10
        })
      ).toThrowErrorMatchingSnapshot();
    });
  });

  describe("convertCoordinateToIndices", () => {
    it("given an invalid coordinate then throw an error", () => {
      // arrange, act, assert
      expect(() =>
        convertCoordinateToIndices("A11")
      ).toThrowErrorMatchingSnapshot();
    });

    it("given a valid coordinate then return the indices", () => {
      // arrange

      // act
      const actualResult = convertCoordinateToIndices("E6");

      // assert
      expect(actualResult).toEqual({
        rowIndex: 4,
        columnIndex: 5
      });
    });
  });

  describe("validateCoordinate", () => {
    const testCases = [
      {
        coordinate: "A1",
        valid: true,
        description: "top-left"
      },
      {
        coordinate: "A10",
        valid: true,
        description: "top-right"
      },
      {
        coordinate: "J10",
        valid: true,
        description: "bottom-right"
      },
      {
        coordinate: "J1",
        valid: true,
        description: "bottom-left"
      },
      {
        coordinate: "A12",
        valid: false,
        description: "beyond the last column"
      },
      {
        coordinate: "a1",
        valid: true,
        description: "case insensitive"
      },
      {
        coordinate: "@1",
        valid: false,
        description: "invalid row"
      },
      {
        coordinate: "K1",
        valid: false,
        description: "beyond the last row"
      },
      {
        coordinate: "A",
        valid: false,
        description: "omitted column"
      },
      {
        coordinate: "11",
        valid: false,
        description: "omitted letter"
      },
      {
        coordinate: 11,
        valid: false,
        description: "parameter as a number"
      },
      {
        coordinate: "ello world",
        valid: false,
        description: "invalid column format"
      }
    ];

    testCases.forEach(({ coordinate, valid, description }) => {
      test(`Given ${coordinate} then return ${valid}; ${description}`, () => {
        expect(validateCoordinate(coordinate)).toEqual(valid);
      });
    });
  });
});
