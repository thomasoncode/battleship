class InvalidCoordinateError extends Error {
  constructor() {
    super(
      "Coordinates must start with a letter between A and J. " +
        "They must also end with a number between 1 and 10." +
        "Examples of valid coordinates are A1, A10, J1, and J10"
    );

    this.name = this.constructor.name;
  }
}

module.exports = InvalidCoordinateError;
