// const makeGame = require("./makeGame");
const { makePlayer } = require("./player");
const { HORIZONTAL } = require("./directions");
const { makeGame } = require("./game");
const fleet = require("./fleet");

const playerOne = makePlayer({
  name: "Bob "
});

const playerTwo = makePlayer({
  name: "Jane "
});

console.clear();

const playerOneCoordinates = [
  {
    shipId: fleet.AIRCRAFT_CARRIER,
    direction: HORIZONTAL,
    coordinate: "A1"
  },
  {
    shipId: fleet.BATTLESHIP,
    direction: HORIZONTAL,
    coordinate: "B1"
  },
  {
    shipId: fleet.CRUISER,
    direction: HORIZONTAL,
    coordinate: "C1"
  },
  {
    shipId: fleet.DESTROYER_ONE,
    direction: HORIZONTAL,
    coordinate: "D1"
  },
  {
    shipId: fleet.DESTROYER_TWO,
    direction: HORIZONTAL,
    coordinate: "E1"
  },
  {
    shipId: fleet.SUBMARINE_ONE,
    direction: HORIZONTAL,
    coordinate: "F1"
  },
  {
    shipId: fleet.SUBMARINE_TWO,
    direction: HORIZONTAL,
    coordinate: "G1"
  }
];

const playerTwoCoordinates = [
  {
    shipId: fleet.AIRCRAFT_CARRIER,
    direction: HORIZONTAL,
    coordinate: "A1"
  },
  {
    shipId: fleet.BATTLESHIP,
    direction: HORIZONTAL,
    coordinate: "B1"
  },
  {
    shipId: fleet.CRUISER,
    direction: HORIZONTAL,
    coordinate: "C1"
  },
  {
    shipId: fleet.DESTROYER_ONE,
    direction: HORIZONTAL,
    coordinate: "D1"
  },
  {
    shipId: fleet.DESTROYER_TWO,
    direction: HORIZONTAL,
    coordinate: "E1"
  },
  {
    shipId: fleet.SUBMARINE_ONE,
    direction: HORIZONTAL,
    coordinate: "F1"
  },
  {
    shipId: fleet.SUBMARINE_TWO,
    direction: HORIZONTAL,
    coordinate: "G1"
  }
];

playerOneCoordinates.forEach(playerOne.addShip);
playerTwoCoordinates.forEach(playerTwo.addShip);

playerOne.beginBattle();
playerTwo.beginBattle();

const game = makeGame({
  playerOne,
  playerTwo
});

game.start();

function printGrid(grid) {
  const numberOfColumns = 10;
  const numberOfRows = 10;

  let columnHeader = " \t";
  for (let columnIndex = 0; columnIndex < numberOfColumns; columnIndex += 1) {
    columnHeader += String(columnIndex + 1);
    columnHeader += "\t";
  }

  console.log(columnHeader + "\n");
  for (let row = 0; row < numberOfRows; row += 1) {
    const rowLetter = String.fromCharCode(row + 65);
    let str = `${rowLetter}\t`;
    for (let column = 0; column < numberOfColumns; column += 1) {
      const id = `${rowLetter}${column + 1}`;
      const value = grid.getValueAtCoordinate(id);

      let symbol = "-";
      if (value) {
        symbol = value.isHit ? "x" : value.shipId[0];
      }

      str += `${symbol}\t`;
    }
    console.log(`${str}\n\n`);
  }
}
