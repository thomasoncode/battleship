const { makeFleet } = require("./fleet");

describe("makeShipBuilder", () => {
  it("Given a fleet then the fleet contains the correct ships", () => {
    // act
    const actualResult = makeFleet();

    // assert
    expect(actualResult).toMatchSnapshot();
  });
});
