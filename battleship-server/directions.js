const HORIZONTAL = "HORIZONTAL";
const VERTICAL = "VERTICAL";
const DIRECTIONS = [HORIZONTAL, VERTICAL];

module.exports = {
  HORIZONTAL,
  VERTICAL,
  DIRECTIONS
};
