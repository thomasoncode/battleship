const { makeGrid } = require("./grid");
const { makeFleet } = require("./fleet");
const playerPrepareForBattle = require("./playerPrepareForBattle");

function makePlayer({ name }) {
  let state = playerPrepareForBattle({
    grid: makeGrid(),
    fleet: makeFleet(),
    changeState
  });

  function changeState(nextState) {
    state = nextState;
  }

  return {
    id: Math.random()
      .toString(36)
      .substr(2, 9),
    name,
    // prepare for battle
    addShip({ shipId, direction, coordinate }) {
      state.addShip({ shipId, direction, coordinate });
    },
    removeShip(shipId) {
      state.removeShip(shipId);
    },
    beginBattle() {
      state.beginBattle();
    },
    // battle
    callOutCoordinate(coordinate) {
      return state.callOutCoordinate(coordinate);
    },
    // confirm state
    canBeginBattle() {
      return state.canBeginBattle();
    },
    canCallOutCoordinate() {
      return state.canCallOutCoordinate();
    },
    isFleetSunk() {
      return state.isFleetSunk();
    }
  };
}

module.exports = {
  makePlayer
};
