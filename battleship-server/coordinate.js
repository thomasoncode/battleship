const InvalidCoordinateError = require("./InvalidCoordinateError");

const asciiOffset = "A".charCodeAt(0);

function convertCoordinateToIndices(coordinate) {
  if (validateCoordinate(coordinate)) {
    return getCoordinateIndices(coordinate);
  }

  throw new InvalidCoordinateError();
}

function convertIndicesToCoordinate({ rowIndex, columnIndex }) {
  if (isAValidRowIndex(rowIndex) && isAValidColumnIndex(columnIndex)) {
    const rowLetter = String.fromCharCode(rowIndex + asciiOffset);
    const columnNumber = columnIndex + 1;
    return `${rowLetter}${columnNumber}`;
  }

  throw new Error(
    "Failed to convert coordinate indices to a coordinate. Indices must be inclusively between zero and nine."
  );
}

function validateCoordinate(coordinate) {
  if (
    typeof coordinate != "string" ||
    (typeof coordinate == "string" && coordinate.length < 2)
  ) {
    return false;
  }

  const { rowIndex, columnIndex } = getCoordinateIndices(coordinate);

  return isAValidRowIndex(rowIndex) && isAValidColumnIndex(columnIndex);
}

function getCoordinateIndices(coordinate) {
  const letter = coordinate.slice(0, 1).toUpperCase();
  const column = coordinate.slice(1);

  // subtract 65 or the ASCII value of "A" so that row "A" is mapped to zero
  // and row "J" is mapped to 9
  const rowIndex = letter.charCodeAt(0) - asciiOffset;
  const columnIndex = Number(column) - 1;

  return {
    rowIndex,
    columnIndex
  };
}

function isAValidRowIndex(rowIndex) {
  return rowIndex >= 0 && rowIndex <= 9;
}

function isAValidColumnIndex(columnIndex) {
  return (
    typeof columnIndex == "number" &&
    Number.isNaN(columnIndex) == false &&
    columnIndex >= 0 &&
    columnIndex <= 9
  );
}

module.exports = {
  validateCoordinate,
  convertCoordinateToIndices,
  convertIndicesToCoordinate
};
