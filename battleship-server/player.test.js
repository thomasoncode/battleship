const { makePlayer } = require("./player");
const playerPrepareForBattle = require("./playerPrepareForBattle");

jest.mock("./playerPrepareForBattle");

describe("makePlayer", () => {
  beforeEach(jest.resetAllMocks);

  it("Makes a player", () => {
    // arrange
    const expectedResult = {
      addShip: expect.any(Function),
      removeShip: expect.any(Function),
      beginBattle: expect.any(Function),
      callOutCoordinate: expect.any(Function),
      canBeginBattle: expect.any(Function),
      canCallOutCoordinate: expect.any(Function),
      isFleetSunk: expect.any(Function),
      name: "name",
      id: expect.any(String)
    };

    // act
    const actualResult = makePlayer({ name: "name" });

    // assert
    expect(actualResult).toEqual(expectedResult);
  });

  it("Setups the current state", () => {
    // arrange
    // act
    makePlayer({ name: "name" });

    // assert
    expect(playerPrepareForBattle).toBeCalledWith({
      grid: expect.any(Object),
      fleet: expect.any(Object),
      changeState: expect.any(Function)
    });
  });

  it("Call state's addShip", () => {
    // arrange
    const mockPlayerPrepareForBattle = makeMockPlayerPrepareForBattle();
    const expectedResult = {
      shipId: "shipId",
      direction: "direction",
      coordinate: "coordinate"
    };

    // act
    makePlayer({ name: "name" }).addShip(expectedResult);

    // assert
    expect(mockPlayerPrepareForBattle.addShip).toBeCalledWith(expectedResult);
  });

  it("Call state's remove ship", () => {
    // arrange
    const mockPlayerPrepareForBattle = makeMockPlayerPrepareForBattle();
    const expectedResult = "shipId";

    // act
    makePlayer({ name: "name" }).removeShip(expectedResult);

    // assert
    expect(mockPlayerPrepareForBattle.removeShip).toBeCalledWith(
      expectedResult
    );
  });

  it("Call state's beginBattle", () => {
    // arrange
    const mockPlayerPrepareForBattle = makeMockPlayerPrepareForBattle();

    // act
    makePlayer({ name: "name" }).beginBattle();

    // assert
    expect(mockPlayerPrepareForBattle.beginBattle).toBeCalled();
  });

  it("Call state's callOutCoordinate", () => {
    // arrange
    const mockPlayerPrepareForBattle = makeMockPlayerPrepareForBattle();
    const returnValue = {};
    mockPlayerPrepareForBattle.callOutCoordinate.mockReturnValue(returnValue);
    const expectedResult = "coordinate";

    // act
    const actualReturnValue = makePlayer({ name: "name" }).callOutCoordinate(
      expectedResult
    );

    // assert
    expect(mockPlayerPrepareForBattle.callOutCoordinate).toBeCalledWith(
      expectedResult
    );

    expect(actualReturnValue).toEqual(returnValue);
  });

  it("Call state's canBeginBattle", () => {
    // arrange
    const mockPlayerPrepareForBattle = makeMockPlayerPrepareForBattle();
    mockPlayerPrepareForBattle.canBeginBattle.mockReturnValue(true);

    // act
    const actualResult = makePlayer({ name: "name" }).canBeginBattle();

    // assert
    expect(mockPlayerPrepareForBattle.canBeginBattle).toBeCalled();
    expect(actualResult).toBeTruthy();
  });

  it("Call state's canCallOutCoordinate", () => {
    // arrange
    const mockPlayerPrepareForBattle = makeMockPlayerPrepareForBattle();
    mockPlayerPrepareForBattle.canCallOutCoordinate.mockReturnValue(true);

    // act
    const actualResult = makePlayer({ name: "name" }).canCallOutCoordinate();

    // assert
    expect(mockPlayerPrepareForBattle.canCallOutCoordinate).toBeCalled();
    expect(actualResult).toBeTruthy();
  });

  it("Call state's isFleet", () => {
    // arrange
    const mockPlayerPrepareForBattle = makeMockPlayerPrepareForBattle();
    mockPlayerPrepareForBattle.isFleetSunk.mockReturnValue(true);

    // act
    const actualResult = makePlayer({ name: "name" }).isFleetSunk();

    // assert
    expect(mockPlayerPrepareForBattle.isFleetSunk).toBeCalled();
    expect(actualResult).toBeTruthy();
  });

  it("Calling change state does not throw an error", () => {
    // arrange
    // act
    makePlayer({ name: "name" });
    playerPrepareForBattle.mock.calls[0][0].changeState();

    // assert; this is the best we can do to verify that everything is connected and working
    expect(playerPrepareForBattle.mock.calls[0][0].changeState).not.toThrow();
  });

  function makeMockPlayerPrepareForBattle() {
    const mockPlayerPrepareForBattle = {
      addShip: jest.fn(),
      removeShip: jest.fn(),
      beginBattle: jest.fn(),
      callOutCoordinate: jest.fn(),
      canBeginBattle: jest.fn(),
      canCallOutCoordinate: jest.fn(),
      isFleetSunk: jest.fn()
    };

    playerPrepareForBattle.mockReturnValue(mockPlayerPrepareForBattle);

    return mockPlayerPrepareForBattle;
  }
});
