const {
  convertCoordinateToIndices,
  convertIndicesToCoordinate
} = require("./coordinate");

const { HORIZONTAL, VERTICAL } = require("./directions");

function makeVerticalRange({ rowIndex, columnIndex, index }) {
  return {
    rowIndex: index + rowIndex,
    columnIndex
  };
}

function makeHorizontalRange({ rowIndex, columnIndex, index }) {
  return {
    rowIndex,
    columnIndex: columnIndex + index
  };
}

function makeRange(makeIndices) {
  return function(coordinate, length) {
    const { rowIndex, columnIndex } = convertCoordinateToIndices(coordinate);

    return Array.from({ length }, function makeCoordinate(_, index) {
      return convertIndicesToCoordinate(
        makeIndices({
          rowIndex,
          columnIndex,
          index
        })
      );
    });
  };
}

const horizontalRange = makeRange(makeHorizontalRange);
const verticalRange = makeRange(makeVerticalRange);

function coordinateRange({ direction, coordinate, numberOfSquares }) {
  switch (direction) {
    case VERTICAL:
      return verticalRange(coordinate, numberOfSquares);
    case HORIZONTAL:
      return horizontalRange(coordinate, numberOfSquares);
    default:
      throw new Error(
        `Can not create a coordinate range. ${direction} is an invalid direction`
      );
  }
}

module.exports = coordinateRange;
