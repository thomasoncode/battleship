function makeShip(description, numberOfSquares, id = description) {
  return {
    numberOfSquares,
    description,
    id,
    symbol: description[0],
    coordinates: []
  };
}

function toFleetLookup(lookup, ship) {
  lookup[ship.id] = ship;
  return lookup;
}

const AIRCRAFT_CARRIER = "Aircraft carrier";
const BATTLESHIP = "Battleship";
const DESTROYER = "Destroyer";
const DESTROYER_ONE = `${DESTROYER} One`;
const DESTROYER_TWO = `${DESTROYER} Two`;
const CRUISER = "Cruiser";
const SUBMARINE = "Submarine";
const SUBMARINE_ONE = `${SUBMARINE} One`;
const SUBMARINE_TWO = `${SUBMARINE} Two`;

function makeFleet() {
  return [
    makeShip(AIRCRAFT_CARRIER, 5),
    makeShip(BATTLESHIP, 4),
    makeShip(CRUISER, 3),
    makeShip(DESTROYER, 2, DESTROYER_ONE),
    makeShip(DESTROYER, 2, DESTROYER_TWO),
    makeShip(SUBMARINE, 1, SUBMARINE_ONE),
    makeShip(SUBMARINE, 1, SUBMARINE_TWO)
  ].reduce(toFleetLookup, {});
}

module.exports = {
  makeFleet,
  AIRCRAFT_CARRIER,
  BATTLESHIP,
  DESTROYER_ONE,
  DESTROYER_TWO,
  CRUISER,
  SUBMARINE_ONE,
  SUBMARINE_TWO
};
