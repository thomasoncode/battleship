const playerEndBattle = require("./playerEndBattle");

function playerBattle({ grid, fleet, changeState }) {
  function addShip() {
    throw new Error("Unable to add a ship while in battle.");
  }
  function removeShip() {
    throw new Error("Unable to remove a ship while in battle.");
  }
  function beginBattle() {
    throw new Error("Unable begin battle while in battle.");
  }
  function canBeginBattle() {
    return false;
  }
  function canCallOutCoordinate() {
    return true;
  }
  function isFleetSunk() {
    return false;
  }
  function callOutCoordinate(coordinate) {
    const value = grid.getValueAtCoordinate(coordinate);
    if (value) {
      const ship = fleet[value.shipId];

      grid.setValueAtCoordinate(
        {
          ...value,
          isHit: true
        },
        coordinate
      );

      const isShipSunk = ship.coordinates
        .map(grid.getValueAtCoordinate)
        .reduce(function isSunk(sunk, { isHit }) {
          return sunk && isHit;
        }, true);

      const isFleetSunk = Object.values(fleet)
        .flatMap(function getCoordinates({ coordinates }) {
          return coordinates;
        })
        .map(grid.getValueAtCoordinate)
        .reduce(function isSunk(sunk, { isHit }) {
          return sunk && isHit;
        }, true);

      if (isFleetSunk) {
        changeState(playerEndBattle());
      }

      return {
        isHit: true,
        description: fleet[value.shipId].description,
        isShipSunk: ship.coordinates
          .map(grid.getValueAtCoordinate)
          .reduce(function isSunk(sunk, { isHit }) {
            return sunk && isHit;
          }, true),
        isFleetSunk,
        isShipSunk
      };
    }

    //
    return {
      isHit: false,
      description: "",
      isShipSunk: false,
      isFleetSunk: false
    };
  }

  return {
    addShip,
    removeShip,
    beginBattle,
    canBeginBattle,
    callOutCoordinate,
    canCallOutCoordinate,
    isFleetSunk
  };
}

module.exports = playerBattle;
