const InvalidCoordinateError = require("./InvalidCoordinateError");
const { validateCoordinate } = require("./coordinate");

function makeGrid() {
  let grid = {};

  function deleteValueAtCoordinate(coordinate) {
    if (validateCoordinate(coordinate)) {
      delete grid[coordinate];
    }
  }

  function getValueAtCoordinate(coordinate) {
    if (validateCoordinate(coordinate)) {
      const value = grid[coordinate];
      return value ? value : null;
    }

    throw new InvalidCoordinateError();
  }

  function setValueAtCoordinate(value, coordinate) {
    if (validateCoordinate(coordinate)) {
      grid[coordinate] = value;
      return;
    }

    throw new InvalidCoordinateError();
  }

  return {
    getValueAtCoordinate,
    setValueAtCoordinate,
    deleteValueAtCoordinate,
    toString
  };
}

module.exports = { makeGrid };
