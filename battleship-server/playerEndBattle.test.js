const playerEndBattle = require("./playerEndBattle");

describe("playerEndBattle", () => {
  // the following methods are not allowed in this state
  it("addShip", () => {
    // arrange
    // act
    // assert
    expect(playerEndBattle().addShip).toThrowError(/^Player's fleet is sunk$/);
  });

  it("removeShip", () => {
    // arrange
    // act
    // assert
    expect(playerEndBattle().removeShip).toThrowError(
      /^Player's fleet is sunk$/
    );
  });

  it("beginBattle", () => {
    // arrange
    // act
    // assert
    expect(playerEndBattle().beginBattle).toThrowError(
      /^Player's fleet is sunk$/
    );
  });

  it("callOutCoordinate", () => {
    // arrange
    // act
    // assert
    expect(playerEndBattle().callOutCoordinate).toThrowError(
      /^Player's fleet is sunk$/
    );
  });

  it("canBeginBattle", () => {
    // arrange
    // act
    // assert
    expect(playerEndBattle().canBeginBattle()).toBeFalsy();
  });

  it("canCallOutCoordinate", () => {
    // arrange
    // act
    // assert
    expect(playerEndBattle().canCallOutCoordinate()).toBeFalsy();
  });

  it("isFleetSunk", () => {
    // arrange
    // act
    // assert
    expect(playerEndBattle().isFleetSunk()).toBeTruthy();
  });
});
