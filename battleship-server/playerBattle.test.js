const playerBattle = require("./playerBattle");
const playerEndBattle = require("./playerEndBattle");

jest.mock("./playerEndBattle");

describe("playerBattle", () => {
  // the following methods are not allowed in this state
  it("addShip", () => {
    // arrange
    // act
    // assert
    expect(
      playerBattle({ grid: mockGrid(), fleet: mockFleet() }).addShip
    ).toThrowError(/Unable to add a ship/);
  });

  it("removeShip", () => {
    // arrange
    // act
    // assert
    expect(
      playerBattle({ grid: mockGrid(), fleet: mockFleet() }).removeShip
    ).toThrowError(/Unable to remove a ship/);
  });

  it("beginBattle", () => {
    // arrange
    // act
    // assert
    expect(
      playerBattle({ grid: mockGrid(), fleet: mockFleet() }).beginBattle
    ).toThrowError(/Unable begin battle/);
  });

  it("canBeginBattle", () => {
    // arrange
    // act
    // assert
    expect(
      playerBattle({ grid: mockGrid(), fleet: mockFleet() }).canBeginBattle()
    ).toBeFalsy();
  });

  it("canCallOutCoordinate", () => {
    // arrange
    // act
    // assert
    expect(
      playerBattle({
        grid: mockGrid(),
        fleet: mockFleet()
      }).canCallOutCoordinate()
    ).toBeTruthy();
  });

  it("isFleetSunk", () => {
    // arrange
    // act
    // assert
    expect(
      playerBattle({
        grid: mockGrid(),
        fleet: mockFleet()
      }).isFleetSunk()
    ).toBeFalsy();
  });

  describe("callOutCoordinate", () => {
    it("Given a coordinate without a ship then return a miss", () => {
      // arrange
      const expectedResult = {
        isHit: false,
        description: "",
        isShipSunk: false,
        isFleetSunk: false
      };

      const grid = mockGrid();
      grid.getValueAtCoordinate.mockReturnValue(null);

      // act
      const actualResult = playerBattle({
        grid,
        fleet: mockFleet()
      }).callOutCoordinate("A1");

      // assert
      expect(actualResult).toEqual(expectedResult);
    });

    it("Given a coordinate with a ship and zero hits then return a hit", () => {
      // arrange
      const expectedResult = {
        isHit: true,
        description: "description",
        isShipSunk: false,
        isFleetSunk: false
      };

      const grid = mockGrid();
      grid.getValueAtCoordinate
        .mockReturnValueOnce({
          shipId: "ship",
          isHit: true
        })
        .mockReturnValue({
          shipId: "ship",
          isHit: false
        });

      // act
      const actualResult = playerBattle({
        grid,
        fleet: mockFleet(),
        changeState: jest.fn()
      }).callOutCoordinate("coordinate");

      // assert
      expect(actualResult).toEqual(expectedResult);
    });

    it("Given a coordinate that will sink a ship then return a sunken ship", () => {
      // arrange
      const expectedResult = {
        isHit: true,
        description: "description",
        isShipSunk: true,
        isFleetSunk: false
      };

      const grid = mockGrid();
      grid.getValueAtCoordinate
        .mockReturnValueOnce({
          shipId: "ship",
          isHit: true
        }) //ship information
        .mockReturnValueOnce({
          shipId: "ship",
          isHit: true
        }) // first hit
        .mockReturnValueOnce({
          shipId: "ship",
          isHit: true
        }) // second hit
        .mockReturnValue({
          shipId: "ship",
          isHit: false
        }); // another ship

      // act
      const actualResult = playerBattle({
        grid,
        fleet: mockFleet(),
        changeState: jest.fn()
      }).callOutCoordinate("coordinate");

      // assert
      expect(actualResult).toEqual(expectedResult);
    });

    it("Given a coordinate that will sink the fleet then return a sunken fleet", () => {
      // arrange
      const expectedResult = {
        isHit: true,
        description: "description",
        isShipSunk: true,
        isFleetSunk: true
      };

      const grid = mockGrid();
      grid.getValueAtCoordinate.mockReturnValue({
        shipId: "ship",
        isHit: true
      }); //ship information

      const changeState = jest.fn();

      // act
      const actualResult = playerBattle({
        grid,
        fleet: mockFleet(),
        changeState
      }).callOutCoordinate("coordinate");

      // assert
      expect(actualResult).toEqual(expectedResult);
      expect(changeState).toBeCalled();
      expect(playerEndBattle).toBeCalled();
    });
  });

  function mockGrid() {
    return {
      getValueAtCoordinate: jest.fn(),
      setValueAtCoordinate: jest.fn()
    };
  }

  function mockFleet() {
    return {
      ship: {
        description: "description",
        coordinates: ["coordinate", "coordinate"]
      },
      anotherShip: {
        description: "another ship",
        coordinates: ["another coordinate"]
      }
    };
  }
});
