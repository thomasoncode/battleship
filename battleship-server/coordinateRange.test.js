const coordinateRange = require("./coordinateRange");

describe("coordinateRange", () => {
  it("given a valid range then return a vertical range", () => {
    // arrange
    const expectedResult = ["A1", "B1", "C1"];

    // act
    const actualResult = coordinateRange({
      direction: "VERTICAL",
      coordinate: "A1",
      numberOfSquares: 3
    });

    // assert
    expect(actualResult).toEqual(expectedResult);
  });

  it("given a invalid range then throw an error", () => {
    // arrange, act, assert
    expect(() =>
      coordinateRange({
        direction: "VERTICAL",
        coordinate: "I1",
        numberOfSquares: 3
      })
    ).toThrowErrorMatchingSnapshot();
  });

  it("given a valid starting coordinate then return a horizontal range", () => {
    // arrange
    const expectedResult = ["A1", "A2", "A3"];

    // act
    const actualResult = coordinateRange({
      direction: "HORIZONTAL",
      coordinate: "A1",
      numberOfSquares: 3
    });

    // assert
    expect(actualResult).toEqual(expectedResult);
  });

  it("given an invalid direction then return an empty ", () => {
    // arrange
    const expectedResult = ["A1", "A2", "A3"];

    // act
    // assert
    expect(() =>
      coordinateRange({
        direction: "NOT VALID",
        coordinate: "A1",
        numberOfSquares: 3
      })
    ).toThrowError(/invalid direction/);
  });
});
